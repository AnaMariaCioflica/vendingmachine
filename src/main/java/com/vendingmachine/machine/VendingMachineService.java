package com.vendingmachine.machine;

import com.vendingmachine.bank.Money;
import com.vendingmachine.exception.NotFullPaidException;
import com.vendingmachine.exception.SoldOutException;
import com.vendingmachine.machine.product.Product;
import com.vendingmachine.machine.storage.Storage;

import java.util.ArrayDeque;
import java.util.Map;
import java.util.Queue;

public class VendingMachineService implements IVendingMachine {
	
	private Storage storage;
	
	public Storage getStorage() {
		return storage;
	}
	
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	
	public Product dispenceProduct(int shelfCode) {
		Map<Integer, Queue<Product>> storageMap = storage.getStorageMap();
		Queue<Product> productQueue = storageMap.get(shelfCode);
		if (productQueue.isEmpty()) {
			throw new SoldOutException("Product is not available.");
		}
		
		Product head = productQueue.poll();
		return head;
	}
	
	@Override
	public boolean payProductPrice(int shelfCode, double amountPayed) {
		Map<Integer, Queue<Product>> storageMap = storage.getStorageMap();
		Queue<Product> productQueue = storageMap.get(shelfCode);
		double productPrice = productQueue.peek().getPrice();
		
		if (amountPayed >= productPrice) {
			// todo return rest method
			dispenceProduct(shelfCode);
			return true;
		} else {
			throw new NotFullPaidException("The amount of money introduced is insufficient");
		}
	}
	
	// todo refactorizam metoda data viitoare
	// metoda care ne calculeaza restul
	@Override
	// amountReceived = 11
	// amountExpected = 6
	// change = 5
	// firstCount = 5%5 = 0
	// 1 bancnota = 5/5
	public Queue<Money> computeChange(double amountReceived,
									  double amountExpected) {
		double totalChange = amountReceived - amountExpected;
		double change = totalChange % 5;
		double noOfCoins = totalChange / 5;
		Queue<Money> coins = new ArrayDeque<>();
		
		if (noOfCoins != 0) {
			// create queue of coins to return
			for (int i = 0; i <= noOfCoins; i++) {
				coins.add(Money.FIVE_DOLLAR);
			}
		}
		if (change == 0) {
			return coins;
		} else {
			change = change % 1;
			noOfCoins = change / 1;
			if (noOfCoins != 0) {
				for (int i = 0; i <= noOfCoins; i++) {
					coins.add(Money.ONE_DOLLAR);
				}
			}
			if (change == 0) {
				return coins;
			} else {
				change = change % 0.5;
				noOfCoins = change / 0.5;
				
				if (noOfCoins != 0) {
					for (int i = 0; i <= noOfCoins; i++) {
						coins.add(Money.FIFTY_CENT);
					}
				}
				if (change == 0) {
					return coins;
				} else {
					change = change % 0.1;
					noOfCoins = change / 0.1;
					
					if (noOfCoins != 0) {
						for (int i = 0; i <= noOfCoins; i++) {
							coins.add(Money.TEN_CENT);
						}
					}
					if (change == 0) {
						return coins;
					}
				}
			}
			
		}
		return coins;
	}
	
	// todo metoda care verifica in  storage daca produsul e disponibil
	// todo metoda care verifica in Bank daca avem rest disponibil
	// todo metoda care valideaza banii introdusi de user
	
	
	@Override
	public boolean validateAmountReceived(Queue<Money> moneyReceived) {
		// parcurgem queue-UL si tot ce e diferit de banii pe care noi ii acceptam, respingem
		// pentru
		return false;
	}
}
